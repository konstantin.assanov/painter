package com.assanov.picture.router;

import com.assanov.picture.service.IService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.List;
import java.util.Map;

import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.Mockito.*;

public class RouterTest {

    Map<String, IService> services = Map.of(
            "S1", mock(IService.class),
            "W3", mock(IService.class));

    Router router;

    @BeforeEach
    void init() {
        router = new Router.Builder()
                .addRoute("S1", services.get("S1"))
                .addRoute("W3", services.get("W3"))
                .build();
    }

    @Test
    void shouldThrowExceptionIfUnknownCommand() {
        Request request = new Request("UK", List.of());

        assertThrows(IllegalArgumentException.class, () ->
                router.execute(request));
    }

    @Test
    void shouldCallRightServiceSuccessfully() {
        List<String> ops = List.of("3","17");
        Request request = new Request("S1", ops);

        router.execute(request);

        verify(services.get("S1")).execute(ops);
    }

}
