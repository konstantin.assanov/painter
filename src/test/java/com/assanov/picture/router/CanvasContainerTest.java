package com.assanov.picture.router;

import com.assanov.picture.model.Canvas;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.Mockito.mock;

public class CanvasContainerTest {

    CanvasContainer container;

    @BeforeEach
    void init() {
        container = new CanvasContainer();
    }

    @Test
    void shouldBeEmptyOnInitialization() {
        assertTrue(container.getCanvas().isEmpty());
    }

    @Test
    void shouldContainCorrectCanvasAfterCanvasSetup() {
        Canvas canvas = mock(Canvas.class);

        container.setCanvas(canvas);

        assertTrue(container.getCanvas().isPresent());

        assertEquals(canvas, container.getCanvas().get());
    }

    @Test
    void shouldKeepLastCanvasOnlySuccessfully() {
        Canvas canvas1 = mock(Canvas.class);
        Canvas canvas2 = mock(Canvas.class);

        container.setCanvas(canvas1);
        container.setCanvas(canvas2);

        assertTrue(container.getCanvas().isPresent());

        assertEquals(canvas2, container.getCanvas().get());
    }

}