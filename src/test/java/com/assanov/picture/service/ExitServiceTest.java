package com.assanov.picture.service;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.List;

import static org.junit.jupiter.api.Assertions.assertFalse;

public class ExitServiceTest {

    ExitService service;

    @BeforeEach
    void init() {
        service = new ExitService();
    }

    @Test
    void shouldReturnFalseOnExecution() {
        assertFalse(service.execute(List.of()));
    }

}
