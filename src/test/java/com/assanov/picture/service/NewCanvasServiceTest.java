package com.assanov.picture.service;

import com.assanov.picture.mappers.ToCanvas;
import com.assanov.picture.model.Canvas;
import com.assanov.picture.router.CanvasContainer;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.List;

import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.Mockito.*;

public class NewCanvasServiceTest {

    NewCanvasService service;
    ToCanvas converter;
    CanvasContainer container;

    @BeforeEach
    void init() {
        converter = mock(ToCanvas.class);
        container = mock(CanvasContainer.class);

        service = new NewCanvasService(converter, container);
    }

    @Test
    void shouldBuildAndSetCanvasSuccessfully() {
        Canvas canvas = mock(Canvas.class);

        when(converter.toData(any())).thenReturn(canvas);

        boolean toContinue = service.execute(List.of());

        assertTrue(toContinue);

        verify(canvas).validate();

        verify(container).setCanvas(canvas);

    }

}
