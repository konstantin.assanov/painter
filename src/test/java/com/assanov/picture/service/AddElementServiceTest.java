package com.assanov.picture.service;

import com.assanov.picture.mappers.IMapper;
import com.assanov.picture.model.AbstractElement;
import com.assanov.picture.model.Canvas;
import com.assanov.picture.router.CanvasContainer;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;
import static org.mockito.Mockito.verify;

public class AddElementServiceTest {

    AddElementService service;

    IMapper<AbstractElement> converter;
    CanvasContainer container;

    @BeforeEach
    void init() {
        converter = mock(IMapper.class);
        container = mock(CanvasContainer.class);

        service = new AddElementService(converter, container);
    }

    @Test
    void shouldAddElementSuccessfully() {
        Canvas canvas = mock(Canvas.class);

        when(container.getCanvas()).thenReturn(Optional.of(canvas));

        AbstractElement element = mock(AbstractElement.class);

        when(converter.toData(any())).thenReturn(element);

        boolean toContinue = service.execute(List.of());

        assertTrue(toContinue);

        verify(element).validate();

        verify(canvas).add(element);

    }

    @Test
    void shouldThrowExceptionIfNoCanvas() {
        when(container.getCanvas()).thenReturn(Optional.empty());

        AbstractElement element = mock(AbstractElement.class);

        when(converter.toData(any())).thenReturn(element);

        assertThrows(IllegalArgumentException.class, () ->
                service.execute(List.of()));
    }

}
