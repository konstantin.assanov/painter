package com.assanov.picture.out;

import com.assanov.picture.view.IView;
import com.assanov.picture.view.View;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class ViewTest {

    IView view;

    @BeforeEach
    void init() {
        view = new View.Builder().setHeight(5).setWidth(5).build();
    }

    @Test
    void shouldBeFalseOnIsFreeIfTooSmallX() {
        assertFalse(view.isAvailable(0, 3, 't'));
    }

    @Test
    void shouldBeFalseOnIsFreeIfTooSmallY() {
        assertFalse(view.isAvailable(4, -2, 't'));
    }

    @Test
    void shouldBeFalseOnIsFreeIfTooBigX() {
        assertFalse(view.isAvailable(6, 3, 't'));
    }

    @Test
    void shouldBeFalseOnIsFreeIfTooBigY() {
        assertFalse(view.isAvailable(2, 6, 't'));
    }

    @Test
    void shouldBeTrueOnIsFreeIfInBounds() {
        assertTrue(view.isAvailable(1, 5, ' '));
    }

    @Test
    void shouldBeFalseOnIsFreeIfColorToBeReplacedIsNotRight() {
        view.draw(3,3, 'x');

        assertFalse(view.isAvailable(3, 3, 'c'));
    }

    @Test
    void shouldBeTrueOnIsFreeIfColorToBeReplacedIsRight() {
        view.draw(3,3, 'c');

        assertTrue(view.isAvailable(3, 3, 'c'));
    }

}