package com.assanov.picture.painter;

import com.assanov.picture.router.Request;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

public class RequestParserTest {

    RequestParser parser;

    @BeforeEach
    void init() {
        parser = new RequestParser();
    }

    @Test
    void shouldThrowExceptionIfBlank() {
        assertThrows(IllegalArgumentException.class, () ->
                parser.parse("       "));
    }

    @Test
    void shouldProcessSingleWordCommandCorrectly() {
        Request request = parser.parse("   DD");

        assertTrue("DD".equals(request.cmd));
        assertEquals(0, request.ops.size());
    }

    @Test
    void shouldProcessMultipleOperandsCorrectly() {
        Request request = parser.parse(" N    -12   cordon");

        assertTrue("N".equals(request.cmd));
        assertEquals(2, request.ops.size());
        assertTrue("-12".equals(request.ops.get(0)));
        assertTrue("cordon".equals(request.ops.get(1)));
    }

}
