package com.assanov.picture;

import com.assanov.picture.painter.IPainter;
import com.assanov.picture.painter.PainterFactory;
import com.assanov.picture.view.IViewReader;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;

public class EndToEndTest {

    PainterFactory factory = new PainterFactory();
    IPainter painter;

    @BeforeEach
    void init() {
        painter = factory.newInstance();
    }

    @Test
    void shouldCreateCanvasSuccessfully() {
        boolean toContinue = painter.execute("C 8 12");
        assertTrue(toContinue);

        Optional<IViewReader> optView = painter.read();
        assertTrue(optView.isPresent());

        IViewReader view = optView.get();

        assertEquals(12, view.height());
        assertEquals(8, view.width());
        for (int y = 1; y <= 12; y++) {
            for (int x = 1; x <= 8; x++)
                assertEquals(' ', view.get(x, y));
        }
    }

    @Test
    void shouldDrawLineSuccessfully() {
        boolean toContinue = painter.execute("C 8 12");
        assertTrue(toContinue);

        toContinue = painter.execute(" L 1  1  8    1");
        assertTrue(toContinue);

        Optional<IViewReader> optView = painter.read();
        assertTrue(optView.isPresent());

        IViewReader view = optView.get();

        assertEquals(12, view.height());
        assertEquals(8, view.width());
        for (int y = 1; y <= 12; y++) {
            for (int x = 1; x <= 8; x++)
                if (y == 1)
                    assertEquals('x', view.get(x, y));
                else
                    assertEquals(' ', view.get(x, y));
        }

    }

    @Test
    void shouldDrawRectangleSuccessfully() {
        boolean toContinue = painter.execute("C 8 12");
        assertTrue(toContinue);

        toContinue = painter.execute(" R 3    3 5   7");
        assertTrue(toContinue);

        Optional<IViewReader> optView = painter.read();

        assertTrue(optView.isPresent());

        IViewReader view = optView.get();

        assertEquals(12, view.height());
        assertEquals(8, view.width());
        for (int y = 1; y <= 12; y++) {
            for (int x = 1; x <= 8; x++)
                if (y == 3 && (x >= 3 && x <= 5)
                        || (y == 7 && (x >= 3 && x <= 5))
                        || (x == 3 && (y > 3 && y < 7))
                        || (x == 5 && (y > 3 && y < 7)))
                    assertEquals('x', view.get(x, y));
                else
                    assertEquals(' ', view.get(x, y));
        }
    }

    @Test
    void shouldFillSuccessfully() {
        boolean toContinue = painter.execute("C 8 12");
        assertTrue(toContinue);

        toContinue = painter.execute(" B 4 4 .");
        assertTrue(toContinue);

        Optional<IViewReader> optView = painter.read();

        assertTrue(optView.isPresent());

        IViewReader view = optView.get();

        assertEquals(12, view.height());
        assertEquals(8, view.width());
        for (int y = 1; y <= 12; y++) {
            for (int x = 1; x <= 8; x++)
                assertEquals('.', view.get(x, y));
        }

    }

    @Test
    void shouldFillSuccessfullyBig1000() {
        boolean toContinue = painter.execute("C 1000 1000");
        assertTrue(toContinue);

        toContinue = painter.execute(" B 500 500 .");
        assertTrue(toContinue);

        Optional<IViewReader> optView = painter.read();

        assertTrue(optView.isPresent());

        IViewReader view = optView.get();

        assertEquals(1000, view.height());
        assertEquals(1000, view.width());
        for (int y = 1; y <= 1000; y++) {
            for (int x = 1; x <= 1000; x++)
                assertEquals('.', view.get(x, y));
        }

    }

    @Test
    void shouldTerminateSuccessfullyOnExitCommand() {
        boolean toContinue = painter.execute(" Q ");

        assertFalse(toContinue);
    }

}