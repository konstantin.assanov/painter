package com.assanov.picture.model;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

public class LineTest {

    Point low = mock(Point.class);
    Point high = mock(Point.class);

    @Test
    void shouldReturnFalseOnLayInIfPointOneIsNotIn() {
        Point p1 = mock(Point.class);
        Point p2 = mock(Point.class);

        when(p1.laysIn(low, high)).thenReturn(false);
        when(p2.laysIn(low, high)).thenReturn(true);

        boolean isIn = new Line(p1, p2).laysIn(low, high);

        assertFalse(isIn);
    }

    @Test
    void shouldReturnFalseOnLayInIfPointTwoIsNotIn() {
        Point p1 = mock(Point.class);
        Point p2 = mock(Point.class);

        when(p1.laysIn(low, high)).thenReturn(true);
        when(p2.laysIn(low, high)).thenReturn(false);

        boolean isIn = new Line(p1, p2).laysIn(low, high);

        assertFalse(isIn);
    }

    @Test
    void shouldReturnTrueOnLayInIfBothPointsAreIn() {
        Point p1 = mock(Point.class);
        Point p2 = mock(Point.class);

        when(p1.laysIn(low, high)).thenReturn(true);
        when(p2.laysIn(low, high)).thenReturn(true);

        boolean isIn = new Line(p1, p2).laysIn(low, high);

        assertTrue(isIn);
    }

    @Test
    void shouldValidateBothPointsOnLineValidation() {
        Point p1 = mock(Point.class);
        Point p2 = mock(Point.class);

        // trivial case to avoid an exception
        when(p1.getX()).thenReturn(1);
        when(p2.getX()).thenReturn(1);
        when(p1.getY()).thenReturn(1);
        when(p2.getY()).thenReturn(1);

        assertDoesNotThrow(() -> new Line(p1, p2).validate());

        verify(p1).validate();
        verify(p2).validate();
    }

    @Test
    void shouldThrowExceptionIfNotVerticalOrHorizontal() {
        Point p1 = mock(Point.class);
        Point p2 = mock(Point.class);

        // trivial case to avoid an exception
        when(p1.getX()).thenReturn(1);
        when(p2.getX()).thenReturn(2);
        when(p1.getY()).thenReturn(3);
        when(p2.getY()).thenReturn(4);

        assertThrows(IllegalArgumentException.class,
                () -> new Line(p1, p2).validate());
    }

}
