package com.assanov.picture.model;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertDoesNotThrow;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class CanvasTest {

    @Test
    void shouldThrowExceptionOnValidateIfNotPositiveWidth() {
        assertThrows(IllegalArgumentException.class, () ->
                new Canvas(0, 10).validate());
    }

    @Test
    void shouldThrowExceptionOnValidateIfNotPositiveHeight() {
        assertThrows(IllegalArgumentException.class, () ->
                new Canvas(10, 0).validate());
    }

    @Test
    void shouldBeOkValidateIfPositiveWidthAndHeight() {
        assertDoesNotThrow(() ->
                new Canvas(1, 1).validate());
    }

    @Test
    void shouldThrowExceptionOnAddIfElementIsNotInCanvas() {
        AbstractElement element = mock(AbstractElement.class);

        when(element.laysIn(any(), any())).thenReturn(false);

        assertThrows(IllegalArgumentException.class, () ->
                new Canvas(10, 0).add(element));
    }

    @Test
    void shouldBeOkOnAddIfElementIsInCanvas() {
        AbstractElement element = mock(AbstractElement.class);

        when(element.laysIn(any(), any())).thenReturn(true);

        assertDoesNotThrow(() ->
                new Canvas(10, 0).add(element));
    }

}