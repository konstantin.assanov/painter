package com.assanov.picture.model;

import com.assanov.picture.view.IView;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;

import static org.mockito.Mockito.*;

public class AbstractElementTest {

    IView view = mock(IView.class);
    AbstractElement element = mock(AbstractElement.class, Mockito.CALLS_REAL_METHODS);

    @Test
    void shouldDrawVerticalLineSuccessfully() {
        Point p1 = new Point(3, 5);
        Point p2 = new Point(3, 8);

        element.drawVertical(p1, p2, view);

        verify(view, times(4)).draw(eq(3), anyInt());

        verify(view).draw(eq(3), eq(5));
        verify(view).draw(eq(3), eq(6));
        verify(view).draw(eq(3), eq(7));
        verify(view).draw(eq(3), eq(8));
    }

    @Test
    void shouldDrawHorizontalLineSuccessfully() {
        Point p1 = new Point(4, 4);
        Point p2 = new Point(6, 4);

        element.drawHorizontal(p1, p2, view);

        verify(view, times(3)).draw(anyInt(), eq(4));

        verify(view).draw(eq(4), eq(4));
        verify(view).draw(eq(5), eq(4));
        verify(view).draw(eq(6), eq(4));
    }

}