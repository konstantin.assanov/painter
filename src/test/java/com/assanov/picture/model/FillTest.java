package com.assanov.picture.model;

import com.assanov.picture.view.IView;
import com.assanov.picture.view.View;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.Mockito.*;

public class FillTest {

    @Test
    void shouldValidateItsPointOnFillValidation() {
        Point p = mock(Point.class);

        new Fill(p, 'c').validate();

        verify(p).validate();
    }

    @Test
    void shouldReturnFalseOnLayInIfPointIsOut() {
        Point p = mock(Point.class);

        when(p.laysIn(any(), any())).thenReturn(false);

        assertFalse(new Fill(p, 'c').laysIn(mock(Point.class), mock(Point.class)));
    }

    @Test
    void shouldReturnTrueOnLayInIfPointIsIn() {
        Point p = mock(Point.class);

        when(p.laysIn(any(), any())).thenReturn(true);

        assertTrue(new Fill(p, 'c').laysIn(mock(Point.class), mock(Point.class)));
    }

    @Test // not completely unit test because counts on real View and Point::neighbours() function
    void shouldFillSuccessfully() {
        IView view = spy(new View.Builder().setHeight(100).setWidth(100).build());

        new Fill(new Point(1,1), '.')
                .drawTo(view);

        verify(view, times(10_000))
                .draw(anyInt(), anyInt(), eq('.'));
    }

}