package com.assanov.picture.model;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

public class PointTest {

    Point topLeft = new Point(2, 2);
    Point bottomRight = new Point(7, 7);

    @Test
    void shouldThrowExceptionOnValidateIfNotPositiveX() {
        assertThrows(IllegalArgumentException.class, () ->
                new Point(0, 10).validate());
    }

    @Test
    void shouldThrowExceptionOnValidateIfNotPositiveY() {
        assertThrows(IllegalArgumentException.class, () ->
                new Point(10, 0).validate());
    }

    @Test
    void shouldBeOkValidateIfPositiveXY() {
        assertDoesNotThrow(() ->
                new Point(10, 10).validate());
    }

    @Test
    void shouldReturnFalseOnLayInIfXIsSmaller() {
        boolean isIn = new Point(1, 5).laysIn(topLeft, bottomRight);

        assertFalse(isIn);
    }

    @Test
    void shouldReturnFalseOnLayInIfXIsBigger() {
        boolean isIn = new Point(10, 5).laysIn(topLeft, bottomRight);

        assertFalse(isIn);
    }

    @Test
    void shouldReturnFalseOnLayInIfYIsSmaller() {
        boolean isIn = new Point(3, 1).laysIn(topLeft, bottomRight);

        assertFalse(isIn);
    }

    @Test
    void shouldReturnFalseOnLayInIfYIsBigger() {
        boolean isIn = new Point(3, 10).laysIn(topLeft, bottomRight);

        assertFalse(isIn);
    }

    @Test
    void shouldReturnTrueOnLayInIfLaysIn() {
        boolean isIn = new Point(3, 5).laysIn(topLeft, bottomRight);

        assertTrue(isIn);
    }

    @Test
    void shouldProducePointNeighbourSuccessfully() {
        Point neighbour = new Point(3, 9).neighbour(-2, 3);

        assertEquals(1, neighbour.getX());
        assertEquals(12, neighbour.getY());
    }

}
