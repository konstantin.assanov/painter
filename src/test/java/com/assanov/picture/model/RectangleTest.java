package com.assanov.picture.model;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.Mockito.*;
import static org.mockito.Mockito.verify;

public class RectangleTest {

    Point low = mock(Point.class);
    Point high = mock(Point.class);

    @Test
    void shouldReturnFalseOnLayInIfPointOneIsNotIn() {
        Point p1 = mock(Point.class);
        Point p2 = mock(Point.class);

        when(p1.laysIn(low, high)).thenReturn(false);
        when(p2.laysIn(low, high)).thenReturn(true);

        boolean isIn = new Rectangle(p1, p2).laysIn(low, high);

        assertFalse(isIn);
    }

    @Test
    void shouldReturnFalseOnLayInIfPointTwoIsNotIn() {
        Point p1 = mock(Point.class);
        Point p2 = mock(Point.class);

        when(p1.laysIn(low, high)).thenReturn(true);
        when(p2.laysIn(low, high)).thenReturn(false);

        boolean isIn = new Rectangle(p1, p2).laysIn(low, high);

        assertFalse(isIn);
    }

    @Test
    void shouldReturnTrueOnLayInIfBothPointsAreIn() {
        Point p1 = mock(Point.class);
        Point p2 = mock(Point.class);

        when(p1.laysIn(low, high)).thenReturn(true);
        when(p2.laysIn(low, high)).thenReturn(true);

        boolean isIn = new Rectangle(p1, p2).laysIn(low, high);

        assertTrue(isIn);
    }

    @Test
    void shouldValidateBothPointsOnRectangleValidation() {
        Point p1 = mock(Point.class);
        Point p2 = mock(Point.class);

        new Rectangle(p1, p2).validate();

        verify(p1).validate();
        verify(p2).validate();
    }

}