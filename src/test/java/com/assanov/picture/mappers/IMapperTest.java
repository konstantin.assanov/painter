package com.assanov.picture.mappers;

import org.junit.jupiter.api.Test;
import org.mockito.Mockito;

import java.util.List;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.mock;

public class IMapperTest {

    IMapper mapper = mock(IMapper.class, Mockito.CALLS_REAL_METHODS);

    @Test
    void shouldThrowExceptionOnGetIfListIsNull() {
        assertThrows(IllegalArgumentException.class, () ->
                mapper.get(null, 1));
    }

    @Test
    void shouldThrowExceptionOnGetIfIndexOutOfListBounds() {
        assertThrows(IllegalArgumentException.class, () ->
                mapper.get(List.of("3", "7"), 2));
    }

    @Test
    void shouldGetSuccessfully() {
        String found = mapper.get(List.of("other", "Operand"), 1);

        assertTrue("Operand".equals(found));
    }

    @Test
    void shouldThrowExceptionOnParseIfNotInteger() {
        assertThrows(IllegalArgumentException.class, () ->
                mapper.parse("g7h"));
    }

    @Test
    void shouldParseSuccessfully() {
        assertEquals(17, mapper.parse("17"));
    }

}
