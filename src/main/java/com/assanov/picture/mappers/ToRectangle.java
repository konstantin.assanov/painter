package com.assanov.picture.mappers;

import com.assanov.picture.model.AbstractElement;
import com.assanov.picture.model.Point;
import com.assanov.picture.model.Rectangle;

import java.util.List;

public class ToRectangle implements IMapper<AbstractElement> {

    @Override
    public AbstractElement toData(List<String> ops) {
        return new Rectangle(
                new Point(toInt(ops, 0), toInt(ops, 1)),
                new Point(toInt(ops, 2), toInt(ops,3)));
    }

}