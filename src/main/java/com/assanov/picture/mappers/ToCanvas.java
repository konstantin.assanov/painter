package com.assanov.picture.mappers;

import com.assanov.picture.model.Canvas;

import java.util.List;

public class ToCanvas implements IMapper<Canvas> {

    @Override
    public Canvas toData(List<String> ops) {
        return new Canvas(toInt(ops, 0), toInt(ops, 1));
    }

}