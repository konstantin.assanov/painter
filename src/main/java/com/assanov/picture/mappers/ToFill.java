package com.assanov.picture.mappers;

import com.assanov.picture.model.Fill;
import com.assanov.picture.model.AbstractElement;
import com.assanov.picture.model.Point;

import java.util.List;

public class ToFill implements IMapper<AbstractElement> {

    @Override
    public AbstractElement toData(List<String> ops) {
        return new Fill(new Point(toInt(ops, 0), toInt(ops, 1)),
                get(ops, 2).charAt(0));
    }

}