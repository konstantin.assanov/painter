package com.assanov.picture.mappers;

import com.assanov.picture.model.AbstractElement;
import com.assanov.picture.model.Line;
import com.assanov.picture.model.Point;

import java.util.List;

public class ToLine implements IMapper<AbstractElement> {

    @Override
    public AbstractElement toData(List<String> ops) {
        return new Line(
                new Point(toInt(ops, 0), toInt(ops, 1)),
                new Point(toInt(ops, 2), toInt(ops,3)));
    }

}