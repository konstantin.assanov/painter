package com.assanov.picture.mappers;

import java.util.List;
import java.util.Optional;

public interface IMapper<T> {

    T toData(List<String> ops);

    default String get(List<String> ops, int index) {
        if (ops == null || index >= ops.size())
            throw new IllegalArgumentException("Invalid number of parameters.");

        return Optional
                .ofNullable(ops.get(index))
                .map(String::trim)
                .orElse("");
    }

    default int parse(String value) {
        try {
            return Integer.parseInt(value);
        } catch(NumberFormatException nfe) {
            throw new IllegalArgumentException("Operand <" + value + "> could not be converted to an integer value.");
        }
    }

    default int toInt(List<String> ops, int index) {
        return parse(get(ops, index));
    }

}