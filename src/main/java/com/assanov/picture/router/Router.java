package com.assanov.picture.router;

import com.assanov.picture.service.IService;

import java.util.HashMap;
import java.util.Map;
import java.util.Optional;

public class Router {

    private final Map<String, IService> services;

    private Router(Map<String, IService> services) {
        this.services = services;
    }

    public boolean execute(Request request) {
        return Optional.ofNullable(services.get(request.cmd))
                .orElseThrow(() -> new IllegalArgumentException("Command <" + request.cmd + "> is unknown."))
                .execute(request.ops);
    }

    public static class Builder {

        private Map<String, IService> services = new HashMap<>();

        public Builder addRoute(String cmd, IService service) {
            services.put(cmd, service);
            return this;
        }

        public Router build() {
            return new Router(services);
        }

    }

}