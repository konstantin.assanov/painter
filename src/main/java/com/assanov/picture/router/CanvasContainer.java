package com.assanov.picture.router;

import com.assanov.picture.model.Canvas;

import java.util.Optional;

public class CanvasContainer {

    private Canvas canvas;

    public Optional<Canvas> getCanvas() {
        return Optional.ofNullable(canvas);
    }

    public void setCanvas(Canvas canvas) {
        this.canvas = canvas;
    }

}
