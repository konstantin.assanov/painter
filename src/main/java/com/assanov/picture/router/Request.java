package com.assanov.picture.router;

import java.util.List;

public class Request {

    public final String cmd;
    public final List<String> ops;

    public Request(String cmd, List<String> ops) {
        this.cmd = cmd;
        this.ops = ops;
    }

}
