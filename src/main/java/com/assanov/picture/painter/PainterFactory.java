package com.assanov.picture.painter;

import com.assanov.picture.mappers.ToCanvas;
import com.assanov.picture.mappers.ToFill;
import com.assanov.picture.mappers.ToLine;
import com.assanov.picture.mappers.ToRectangle;
import com.assanov.picture.view.IViewBuilder;
import com.assanov.picture.view.IViewReader;
import com.assanov.picture.view.View;
import com.assanov.picture.router.CanvasContainer;
import com.assanov.picture.router.Router;
import com.assanov.picture.service.AddElementService;
import com.assanov.picture.service.ExitService;
import com.assanov.picture.service.NewCanvasService;

import java.util.Optional;

public class PainterFactory {

    public IPainter newInstance() {
        return new PainterImpl();
    }

    private class PainterImpl implements IPainter {

        private final RequestParser parser = new RequestParser();
        private final CanvasContainer container = new CanvasContainer();
        private final IViewBuilder viewBuilder = new View.Builder();

        private final Router router = new Router.Builder()
                    .addRoute("Q", new ExitService())
                    .addRoute("C", new NewCanvasService(new ToCanvas(), container))
                    .addRoute("L", new AddElementService(new ToLine(), container))
                    .addRoute("R", new AddElementService(new ToRectangle(), container))
                    .addRoute("B", new AddElementService(new ToFill(), container))
                    .build();

        private PainterImpl() {}

        @Override
        public boolean execute(String request) {
            return router.execute(parser.parse(request));
        }

        @Override
        public Optional<IViewReader> read() {
            return container.getCanvas()
                    .map(canvas -> canvas.drawTo(viewBuilder));
        }

    }

}