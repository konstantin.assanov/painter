package com.assanov.picture.painter;

import com.assanov.picture.router.Request;

import java.util.Arrays;
import java.util.stream.Collectors;

public class RequestParser {

    public Request parse(String str) {
        if (str.isBlank())
            throw new IllegalArgumentException("Undefined command");

        String[] req = str.trim().split(" ");

        return new Request(
                req[0],
                Arrays.stream(req, 1, req.length)
                        .filter(op -> !op.isBlank())
                        .collect(Collectors.toList()));
    }

}
