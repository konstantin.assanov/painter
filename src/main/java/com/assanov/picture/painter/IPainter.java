package com.assanov.picture.painter;

import com.assanov.picture.view.IViewReader;

import java.util.Optional;

public interface IPainter {

    boolean execute(String request);

    Optional<IViewReader> read();

}