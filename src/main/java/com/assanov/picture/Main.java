package com.assanov.picture;

import com.assanov.picture.output.ViewRenderer;
import com.assanov.picture.painter.IPainter;
import com.assanov.picture.painter.PainterFactory;

public class Main {

    public static void main(String[] args) {

        IPainter painter = new PainterFactory().newInstance();
        ViewRenderer renderer = new ViewRenderer();

        boolean isOpenSession = true;

        while (isOpenSession) {
            System.out.print("> ");
            String cmd = System.console().readLine().trim();

            try {
                isOpenSession = painter.execute(cmd);
            } catch(RuntimeException re) {
                System.out.println("Command could not be processed:");
                System.out.println(re.getMessage());
                continue;
            }

            painter.read().ifPresent(view ->
                    System.out.println(renderer.asString(view)));
        }

    }

}
