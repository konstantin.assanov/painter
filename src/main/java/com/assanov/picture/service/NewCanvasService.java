package com.assanov.picture.service;

import com.assanov.picture.mappers.ToCanvas;
import com.assanov.picture.model.Canvas;
import com.assanov.picture.router.CanvasContainer;

import java.util.List;

public class NewCanvasService implements IService {

    private final ToCanvas converter;
    private final CanvasContainer container;

    public NewCanvasService(ToCanvas converter, CanvasContainer container) {
        this.converter = converter;
        this.container = container;
    }

    public boolean execute(List<String> ops) {
        Canvas canvas = converter.toData(ops);
        canvas.validate();
        container.setCanvas(canvas);

        return true;
    }

}