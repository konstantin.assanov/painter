package com.assanov.picture.service;

import java.util.List;

public interface IService {

    boolean execute(List<String> ops);

}