package com.assanov.picture.service;

import com.assanov.picture.mappers.IMapper;
import com.assanov.picture.model.AbstractElement;
import com.assanov.picture.router.CanvasContainer;

import java.util.List;

public class AddElementService implements IService {

    private final IMapper<AbstractElement> converter;
    private final CanvasContainer container;

    public AddElementService(IMapper<AbstractElement> converter, CanvasContainer container) {
        this.converter = converter;
        this.container = container;
    }

    public boolean execute(List<String> ops) {
        AbstractElement element = converter.toData(ops);
        element.validate();

        container.getCanvas()
                .orElseThrow(() -> new IllegalArgumentException("There is no canvas. A canvas should be created first."))
                .add(element);

        return true;
    }

}
