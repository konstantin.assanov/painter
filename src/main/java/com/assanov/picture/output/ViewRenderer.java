package com.assanov.picture.output;

import com.assanov.picture.view.IViewReader;

import java.util.stream.Collectors;
import java.util.stream.IntStream;

public class ViewRenderer {

    public String asString(IViewReader view) {
        String str = "";
        String horizontal = " " + "_".repeat(2 * view.width() + 1);

        str += horizontal + "\n";

        for (int i = 0; i < view.height(); i++)
            str += line(view, i + 1) + "\n";

        str += horizontal + "\n";

        return str;
    }

    private String line(IViewReader view, int y) {
        String str = IntStream.rangeClosed(1, view.width())
                .mapToObj(x -> "" + view.get(x, y))
                .collect(Collectors.joining(" ", " ", " "));

        return "|" + str + "|";
    }

}