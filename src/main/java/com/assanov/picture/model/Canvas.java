package com.assanov.picture.model;

import com.assanov.picture.view.IView;
import com.assanov.picture.view.IViewBuilder;
import com.assanov.picture.view.IViewReader;

import java.util.ArrayList;
import java.util.List;

public class Canvas implements IValidate {

    private final int width;
    private final int height;

    private final List<AbstractElement> list = new ArrayList<>();

    public Canvas(int width, int height) {
        this.width = width;
        this.height = height;
    }

    public void add(AbstractElement element) {
        if (!element.laysIn(new Point(1, 1), new Point(width, height)))
            throw new IllegalArgumentException("Element is out of the canvas bounds.");

        list.add(element);
    }

    @Override
    public void validate() {
        if (width < 1 || height < 1)
            throw new IllegalArgumentException("Canvas dimensions (" + width + "," + height + ") must be positive.");
    }

    public IViewReader drawTo(IViewBuilder builder) {
        IView view = builder.setWidth(width).setHeight(height).build();
        list.forEach(element -> element.drawTo(view));
        return view;
    }

}