package com.assanov.picture.model;

import com.assanov.picture.view.IView;

public abstract class AbstractElement implements IValidate {

    abstract boolean laysIn(Point low, Point high);

    abstract void drawTo(IView view);

    void drawVertical(Point p1, Point p2, IView view) {
        Point begin = p1;
        Point end = p2;
        if (p1.getY() > p2.getY()) {
            begin = p2;
            end = p1;
        }

        for (int y = begin.getY(); y <= end.getY(); y++)
            view.draw(begin.getX(), y);
    }

    void drawHorizontal(Point p1, Point p2, IView view) {
        Point begin = p1;
        Point end = p2;
        if (p1.getX() > p2.getX()) {
            begin = p2;
            end = p1;
        }

        for (int x = begin.getX(); x <= end.getX(); x++)
            view.draw(x, begin.getY());
    }

}
