package com.assanov.picture.model;

import com.assanov.picture.view.IView;

public class Rectangle extends AbstractElement {

    private Point p1;
    private Point p2;

    public Rectangle(Point p1, Point p2) {
        this.p1 = p1;
        this.p2 = p2;
    }

    @Override
    public void validate() {
        p1.validate();
        p2.validate();
    }

    @Override
    public boolean laysIn(Point low, Point high) {
        return p1.laysIn(low, high) && p2.laysIn(low, high);
    }

    @Override
    public void drawTo(IView view) {
        Point p12 = new Point(p2.getX(), p1.getY());
        Point p21 = new Point(p1.getX(), p2.getY());

        drawHorizontal(p1, p12, view);
        drawHorizontal(p21, p2, view);

        drawVertical(p12, p2, view);
        drawVertical(p1, p21, view);
    }

}