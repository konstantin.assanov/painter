package com.assanov.picture.model;

import com.assanov.picture.view.IView;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class Fill extends AbstractElement {

    private Point p;
    private char color;

    public Fill(Point p, char color) {
        this.p = p;
        this.color = color;
    }

    @Override
    public void validate() {
        p.validate();
    }

    @Override
    public boolean laysIn(Point low, Point high) {
        return p.laysIn(low, high);
    }

    @Override
    public void drawTo(IView view) {
        char colorToBeReplaced = view.get(p.getX(), p.getY());

        availablePoints = new ArrayList<>();
        availablePoints.add(p);

        do {
            drawNeighbours(view, colorToBeReplaced);
        } while (!availablePoints.isEmpty());
    }

    private List<Point> availablePoints;

    void drawNeighbours(IView view, char colorToBeReplaced) {
        availablePoints.forEach(neighbour ->
                view.draw(neighbour.getX(), neighbour.getY(), color));

        availablePoints = availablePoints
                .stream()
                .flatMap(point -> point.neighbours()
                        .stream()
                        .filter(n -> view.isAvailable(n.getX(), n.getY(), colorToBeReplaced)))
                .distinct()
                .collect(Collectors.toList());
    }

}