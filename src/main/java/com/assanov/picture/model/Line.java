package com.assanov.picture.model;

import com.assanov.picture.view.IView;

public class Line extends AbstractElement {

    private Point p1;
    private Point p2;

    public Line(Point p1, Point p2) {
        this.p1 = p1;
        this.p2 = p2;
    }

    @Override
    public void validate() {
        p1.validate();
        p2.validate();

        if (p1.getX() != p2.getX()
                && p1.getY() != p2.getY())
            throw new IllegalArgumentException("Only horizontal or vertical lines are supported.");
    }

    @Override
    public boolean laysIn(Point low, Point high) {
        return p1.laysIn(low, high) && p2.laysIn(low, high);
    }

    @Override
    public void drawTo(IView view) {
        if (p1.getX() == p2.getX()) // vertical
            drawVertical(p1, p2, view);
        else // horizontal
            drawHorizontal(p1, p2, view);
    }

}