package com.assanov.picture.model;

import com.assanov.picture.view.IView;
import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.Setter;

import java.util.List;

@Data
@AllArgsConstructor
@Setter(AccessLevel.NONE)
public class Point extends AbstractElement {

    private final int x;
    private final int y;

    @Override
    public void validate() {
        if (x < 1 || y < 1)
            throw new IllegalArgumentException("Point coordinates (" + x + "," + y + ") must be positive.");
    }

    @Override
    public boolean laysIn(Point low, Point high) {
        if (x < low.getX() || x > high.getX())
            return false;

        if (y < low.getY() || y > high.getY())
            return false;

        return true;
    }

    @Override
    public void drawTo(IView view) {
        view.draw(x, y);
    }

    List<Point> neighbours() {
        return List.of(
                neighbour(-1, -1),
                neighbour(0, -1),
                neighbour(1, -1),
                neighbour(1, 0),
                neighbour(1, 1),
                neighbour(0, 1),
                neighbour(-1, 1),
                neighbour(-1, 0)
        );
    }

    Point neighbour(int deltaX, int deltaY) {
        return new Point(x + deltaX, y + deltaY);
    }

}