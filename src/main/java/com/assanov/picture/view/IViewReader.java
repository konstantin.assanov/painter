package com.assanov.picture.view;

public interface IViewReader {

    int height();

    int width();

    char get(int x, int y);

}