package com.assanov.picture.view;

public interface IView extends IViewReader {

    boolean isAvailable(int x, int y, char colorToBeReplaced);

    void draw(int x, int y);

    void draw(int x, int y, char color);

}