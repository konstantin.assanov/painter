package com.assanov.picture.view;

public class View implements IView {

    private final int width;
    private final int height;
    private final char[][] table; //(height, width) = (y, x)

    private View(int width, int height) {
        this.width = width;
        this.height = height;
        table = new char[height][width];
        for (int i = 0; i < height; i++)
            for (int j = 0; j < width; j++)
                table[i][j] = ' ';
    }

    @Override
    public int width() {
        return width;
    }

    @Override
    public int height() {
        return height;
    }

    @Override
    public char get(int x, int y) {
        return table[y - 1][x - 1];
    }

    @Override
    public boolean isAvailable(int x, int y, char colorToBeReplaced) {
        if (x < 1 || y < 1 || x > width || y > height)
            return false;

        return table[y - 1][x - 1] == colorToBeReplaced;
    }

    @Override
    public void draw(int x, int y) {
        table[y - 1][x - 1] = 'x';
    }

    @Override
    public void draw(int x, int y, char color) {
        table[y - 1][x - 1] = color;
    }

    public static class Builder implements IViewBuilder {

        private int width;
        private int height;

        public Builder setWidth(int width) {
            this.width = width;
            return this;
        }

        public Builder setHeight(int height) {
            this.height = height;
            return this;
        }

        public IView build() {
            return new View(width, height);
        }

    }

}