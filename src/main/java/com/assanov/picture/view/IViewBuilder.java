package com.assanov.picture.view;

public interface IViewBuilder {

    IViewBuilder setWidth(int width);

    IViewBuilder setHeight(int height);

    public IView build();

}