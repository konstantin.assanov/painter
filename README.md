# Painter

## 0. Overview

A simple console version of a drawing program.<br>
At this time, the functionality of the program is quite limited but this might change in the future.<br>

In a nutshell, the program works as follows: <br>
 1. Create a new canvas
 2. Start drawing on the canvas by issuing various commands
 3. Quit

```
Command         Description
C w h           Create a new canvas of width w and height h.
L x1 y1 x2 y2   Create a new line from (x1,y1) to (x2,y2). Currently only 
                horizontal or vertical lines are supported. Horizontal and vertical lines 
                will be drawn using the 'x' character.
R x1 y1 x2 y2   Create a new rectangle, whose upper left corner is (x1,y1) and
                lower right corner is (x2,y2). Horizontal and vertical lines will be drawn 
                using the 'x' character. 
B x y c         Fill the entire area connected to (x,y) with "colour" c. The 
                behavior of this is the same as that of the "bucket fill" tool in paint 
                programs. 
Q               Quit the program.
```

## 1. Example of Use

[Paint Example](./Example.txt)

[Fill Behavior](./FillBehaviourExample.txt)

## 2. Run

    java com.assanov.picture.Main

## 3. Packages (com.assanov.picture)

1/ **mappers**

Simple converters of the sets of command operands to the corresponding models (Canvas, Line, Rectangle etc.)

2/ **model**

Data models of objects to be rendered in the picture.<br>
They implements IValidate interface.

3/ **output**

Renders the resulting picture as a String for further console display.

4/ **painter**

Main Class - implementation of IPainter that is to be instantiated with a help of PainterFactory.<br>
Also, RequestParser - for initial command parsing.

5/ **router**

Router - routes the Command to the execution by the corresponding service (IService).

6/ **service**

Services that execute the commands:
- AddElementService - add the elements into the picture (Line, Rectangle, Fill)
- NewCanvasService - create a new canvas
- ExitService - leave the painting session.
    
All services implement IService interface.

7/ **view**

View is a class where the Elements on a Canvas are mapped in a form close to rendering.<br>
The View class is a 'materialized view' of the picture with all elements.

## 4. Functioning

***Request:***

1/ Request String is submitted to **IPainter** instance  

2/ Request String is parsed by **RequestParser** to identify the Command

3/ **Router** routes the Command to the corresponding **IService**

4/ **IService** implementation will convert the command operands into the *models* (Line, Rectangle, Fill, Canvas)  
    with the help of the *mappers* (ToCanvas, ToFill, ToLine, ToRectangle)

5/ 3 type of services are possible in the current implementation:<br>
   5.1/ **AddElementService** - convert, validate, and add to picture the elements (Line, Rectangle, Fill)<br>
   5.2/ **NewCanvasService** - convert, validate, create a new **Canvas** and set it into **CanvasContainer**<br>
   5.3/ **ExitService** - manages the painting session termination<br>

6/ If there is a problem of processing an exception is raised;
   Otherwise returns 'true' to continue the painting session,
   and 'false' if the session is to be terminated.

***Response:***

1/ Obtain the current **IViewReader** that represents the current **IView** for read-only access by calling the *read()* of **IPainter**<br>  
2/ **IPainter** will fill the **View** with the data from **Canvas**<br>
3/ Then, Use **ViewRenderer** to create a String representation of **IViewReader**

## 5. Validation & Exception handling

Requests are validated, and, if the request is considered invalid, the Exceptions are raised
with the explanation messages, which can be displayed directly to the end user.

The following exceptions are processed:<br>

1/ *Blank string as request* is entered: exception is raised by **RequestParser**  

2/ *Unknown command*: exception is raised by **Router** if the command code is unknown

3/ *Operands parsing failure*: **Missing parameters** or **Parameter parsing failed** -
    This type of exceptions is raised by converters to data models (**mappers** package)

4/ *Model data validation* - models are subject to constrains, e.g. **Canvas** has to have  
    non-zero width and height  

5/ *Element has to lay fully inside the Canvas' bounds* - the exception is raised if element  
   is out of Canvas' bounds when trying to add to **Canvas**  

6/ *Canvas has to be created first* - the exception is raised when trying to add an element,  
if there is no Canvas defined yet.

NB: Currently, the 'fill' command that is put on the line's point ('x') is simply ignored,
but an exception should be rather raised.

So, the validation can be divided into the following categories:<br>

1/ Validation of the request: correct format, correct operands, and known command  
2/ Validation of the Models per se  
3/ Validation of the Models applicability toward Canvas and other models  
4/ Correct commands order

## 6. Drawing Assumptions

Canvas:<br>
1/ Canvas has to have the positive dimensions both<br>
2/ No element can be added if Canvas undefined<br>
3/ New Canvas will replace the previous in the current painting session<br>

Line & Rectangle:<br>
1/ Line & Rectangle have to lay fully inside the Canvas' bounds<br>
2/ 1-point length Lines & Rectangles are allowed<br>
3/ Line & Rectangle can be placed 'over' any previous element (Line, Rectangle, Fill)<br>

Fill operation:<br>
1/ 'Fill'-operation outside of Canvas' bounds is considered invalid<br>
2/ 'Fill' placed on the Line or Rectangle borders ('x'-points) is changing the color of the borders <br>
3/ New Fill-operation will draw 'over' the previous Fill colors<br>

## 7. Extensibility

1/ New Elements can be managed

Other elements can be managed: Circle, Triangles etc.

2/ New Properties for Element can be managed

Other properties for the elements can be added: e.g. Color for Lines, Width of Lines

3/ New Operations can be added

New operations, like *Move*, *Delete*, *Change Line Length* can be added

4/ Several Canvas can be managed during the same session

Canvas Container can be extended to manage several Canvas

5/ View Rendering can be modified

## 8. Unit Tests & End-To-End Tests

Unit Tests and End-To-End Tests are included.

## 9. Updates

[Updates.pdf](./Updates.pdf) contains the description of the updates included or possible to be included into the current version:  
1/ Recursive 'fill with color' function has bben trasnformed into an iterative function (included into the current implementation)<br>
2/ Coloring of the borders is allowed now (included into the current implementation) <br>
3/ 'Undo' command (as well as 'Redo') function could be added (not implemented in the current version)